package com.castello.walletclient.grpc;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.castello.walletclient.request.WalletClientRequest;
import com.castello.walletclient.response.TransactionsSecondResponse;
import com.castello.walletclient.runnable.RoundRunnable;
import com.castello.walletserver.grpc.WalletServerServiceGrpc.WalletServerServiceBlockingStub;
import com.google.protobuf.Empty;

import net.devh.boot.grpc.client.inject.GrpcClient;

@Service
public class WalletClientService {

    @GrpcClient("grpc-server")
    private WalletServerServiceBlockingStub walletStub;

    @Async
    public void wallet(WalletClientRequest walletClientRequest) {
        IntStream.range(0, walletClientRequest.getNumberOfUsers())
                .parallel()
                .forEach(x -> {
                    ExecutorService executorService = Executors
                            .newFixedThreadPool(walletClientRequest.getNumberOfRequests());
                    IntStream.range(0, walletClientRequest.getNumberOfRequests())
                            .parallel()
                            .forEach(y -> {
                                String userId = UUID.randomUUID().toString();
                                RoundRunnable roundRunnable = RoundRunnable.builder()
                                        .walletStub(walletStub)
                                        .qtdRound(walletClientRequest.getNumberOfRounds())
                                        .userId(userId)
                                        .build();
                                executorService.execute(roundRunnable);
                            });
                    executorService.shutdown();
                });
    }

    public List<TransactionsSecondResponse> tps() {
        List<TransactionsSecondResponse> tps = new ArrayList<>();
        walletStub.tps(Empty.newBuilder().build()).forEachRemaining(x -> {
            TransactionsSecondResponse transactionsSecondResponse = TransactionsSecondResponse.builder()
                    .count(x.getCount())
                    .time(x.getTime())
                    .build();
            tps.add(transactionsSecondResponse);
        });
        return tps.stream()
                .sorted(Comparator.comparing(TransactionsSecondResponse::getCount, Comparator.reverseOrder()))
                .collect(Collectors.toList());

    }

}
