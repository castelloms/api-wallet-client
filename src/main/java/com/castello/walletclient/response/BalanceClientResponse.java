package com.castello.walletclient.response;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class BalanceClientResponse {

    private String currency;
    private double amount;

}
