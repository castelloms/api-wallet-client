package com.castello.walletclient.response;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class TransactionsSecondResponse {

    private int count;
    private String time;

}
