package com.castello.walletclient.request;

import com.castello.walletserver.grpc.Currency;

import lombok.Data;

@Data
public class DepositWithdrawClientRequest {

    private Currency currency;
    private double amount;

}
