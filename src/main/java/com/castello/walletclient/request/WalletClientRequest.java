package com.castello.walletclient.request;

import lombok.Data;

@Data
public class WalletClientRequest {

    private int numberOfUsers;
    private int numberOfRequests;
    private int numberOfRounds;

}
