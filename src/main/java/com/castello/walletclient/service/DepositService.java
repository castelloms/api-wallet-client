package com.castello.walletclient.service;

import org.springframework.stereotype.Service;

import com.castello.walletclient.request.DepositWithdrawClientRequest;
import com.castello.walletserver.grpc.DepositWithdrawRequest;
import com.castello.walletserver.grpc.WalletServerServiceGrpc.WalletServerServiceBlockingStub;

import net.devh.boot.grpc.client.inject.GrpcClient;

@Service
public class DepositService {

    @GrpcClient("grpc-server")
    private WalletServerServiceBlockingStub walletStub;

    public String deposit(String userId, DepositWithdrawClientRequest request) {
        DepositWithdrawRequest depositWithdrawRequest = DepositWithdrawRequest.newBuilder()
                .setUserId(userId)
                .setAmount(request.getAmount())
                .setCurrency(request.getCurrency())
                .build();
        return walletStub.deposit(depositWithdrawRequest).getId();
    }

}
