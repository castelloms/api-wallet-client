package com.castello.walletclient.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.castello.walletclient.response.BalanceClientResponse;
import com.castello.walletserver.grpc.BalanceRequest;
import com.castello.walletserver.grpc.WalletServerServiceGrpc.WalletServerServiceBlockingStub;

import net.devh.boot.grpc.client.inject.GrpcClient;

@Service
public class BalanceService {

    @GrpcClient("grpc-server")
    private WalletServerServiceBlockingStub walletStub;

    public List<BalanceClientResponse> balance(String userId) {
        BalanceRequest balanceRequest = BalanceRequest.newBuilder()
                .setUserId(userId)
                .build();
        List<BalanceClientResponse> clientResponse = new ArrayList<>();
        walletStub.balance(balanceRequest).forEachRemaining(balance -> {
            BalanceClientResponse balanceClientResponse = BalanceClientResponse.builder()
                    .currency(balance.getBalance().getCurrency().name())
                    .amount(balance.getBalance().getAmount())
                    .build();
            clientResponse.add(balanceClientResponse);
        });
        return clientResponse;

    }

}
