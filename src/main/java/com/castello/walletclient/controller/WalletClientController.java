package com.castello.walletclient.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.castello.walletclient.grpc.WalletClientService;
import com.castello.walletclient.request.DepositWithdrawClientRequest;
import com.castello.walletclient.request.WalletClientRequest;
import com.castello.walletclient.response.BalanceClientResponse;
import com.castello.walletclient.response.TransactionsSecondResponse;
import com.castello.walletclient.service.BalanceService;
import com.castello.walletclient.service.DepositService;
import com.castello.walletclient.service.WithdrawService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/wallet")
public class WalletClientController {

    private final WalletClientService walletService;
    private final DepositService depositService;
    private final WithdrawService withdrawService;
    private final BalanceService balanceService;

    @GetMapping(path = "/tps", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<TransactionsSecondResponse>> tps() {
        List<TransactionsSecondResponse> tps = walletService.tps();
        return ResponseEntity.ok(tps);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> wallet(@RequestBody WalletClientRequest request) {
        walletService.wallet(request);
        return ResponseEntity.accepted().build();
    }

    @PostMapping(path = "/{userId}/deposit", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> deposit(@PathVariable String userId,
            @RequestBody DepositWithdrawClientRequest request) {
        String id = depositService.deposit(userId, request);
        return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(id)
                .toUri())
                .build();
    }

    @PostMapping(path = "/{userId}/withdraw", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> withdraw(@PathVariable String userId,
            @RequestBody DepositWithdrawClientRequest request) {
        String id = withdrawService.withdraw(userId, request);
        return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(id)
                .toUri())
                .build();
    }

    @GetMapping(path = "/{userId}/balance", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BalanceClientResponse>> balance(@PathVariable String userId) {
        List<BalanceClientResponse> listBalance = balanceService.balance(userId);
        return ResponseEntity.ok(listBalance);
    }

}
