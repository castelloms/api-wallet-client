package com.castello.walletclient.runnable;

import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import com.castello.walletserver.grpc.BalanceRequest;
import com.castello.walletserver.grpc.Currency;
import com.castello.walletserver.grpc.DepositWithdrawRequest;
import com.castello.walletserver.grpc.WalletServerServiceGrpc.WalletServerServiceBlockingStub;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
public class RoundRunnable implements Runnable {

    private WalletServerServiceBlockingStub walletStub;
    private int qtdRound;
    private String userId;

    @Override
    public void run() {
        randomRound(qtdRound, userId);
    }

    private void randomRound(int qtdRound, String userId) {
        if (qtdRound == 0) {
            log.warn("No rounds to execute, {}", qtdRound);
            return;
        }
        List<Integer> rounds = List.of(1, 2, 3);
        IntStream.range(0, qtdRound)
                .forEach(x -> {
                    int roundSelect = new Random().nextInt(rounds.size());
                    executeRound(roundSelect, userId);
                });

    }

    private void executeRound(int round, String userId) {
        if (round == 0) {
            roundA(userId);
        } else if (round == 1) {
            roundB(userId);
        } else {
            roundC(userId);
        }
    }

    private void roundA(String userId) {
        try {
            DepositWithdrawRequest request = DepositWithdrawRequest.newBuilder()
                    .setUserId(userId)
                    .setCurrency(Currency.USD)
                    .setAmount(100)
                    .build();
            walletStub.deposit(request);
            request = DepositWithdrawRequest.newBuilder(request)
                    .setAmount(200)
                    .build();
            walletStub.withdraw(request);
            request = DepositWithdrawRequest.newBuilder(request)
                    .setAmount(100)
                    .setCurrency(Currency.USD)
                    .build();
            walletStub.deposit(request);
            BalanceRequest balanceRequest = BalanceRequest.newBuilder()
                    .setUserId(userId)
                    .build();
            walletStub.balance(balanceRequest);
            request = DepositWithdrawRequest.newBuilder(request)
                    .setAmount(100)
                    .build();
            walletStub.withdraw(request);
            walletStub.balance(balanceRequest);
            walletStub.withdraw(request);
        } catch (Exception e) {
            log.warn("Error on round A: {}", e);
        }
    }

    private void roundB(String userId) {
        try {
            DepositWithdrawRequest request = DepositWithdrawRequest.newBuilder()
                    .setUserId(userId)
                    .setCurrency(Currency.GBP)
                    .setAmount(100)
                    .build();
            walletStub.withdraw(request);
            request = DepositWithdrawRequest.newBuilder(request)
                    .setAmount(300)
                    .build();
            walletStub.deposit(request);
            request = DepositWithdrawRequest.newBuilder(request)
                    .setAmount(100)
                    .build();
            walletStub.withdraw(request);
            walletStub.withdraw(request);
            walletStub.withdraw(request);
        } catch (Exception e) {
            log.warn("Error on round B: {}", e);
        }
    }

    private void roundC(String userId) {
        try {
            BalanceRequest balanceRequest = BalanceRequest.newBuilder()
                    .setUserId(userId)
                    .build();
            walletStub.balance(balanceRequest);
            DepositWithdrawRequest request = DepositWithdrawRequest.newBuilder()
                    .setUserId(userId)
                    .setCurrency(Currency.USD)
                    .setAmount(100)
                    .build();
            walletStub.deposit(request);
            walletStub.deposit(request);
            walletStub.withdraw(request);
            walletStub.deposit(request);
            walletStub.balance(balanceRequest);
            request = DepositWithdrawRequest.newBuilder(request)
                    .setAmount(200)
                    .build();
            walletStub.withdraw(request);
            walletStub.balance(balanceRequest);
        } catch (Exception e) {
            log.warn("Error on round C: {}", e);
        }
    }

}
