FROM openjdk:9-jre
WORKDIR /wallet-client
COPY ./target/api-wallet-client.jar /wallet/app.jar
CMD ["/usr/bin/java","-jar","/wallet/app.jar"]
