# API Wallet Client
Project Wallet Client

## Prerequisites

 - JDK 9+
 - Docker 18.02.0+ (docker-compose 1.22.0+)

## Initialize Project

**Clone project**

```sh
git clone https://bitbucket.org/castelloms/api-wallet-client.git
```

**Execute this commands on the project folder - Docker**

```sh
In the folder where is pom.xml, execute:
- generate-image.sh - build project with your dependencies and generate docker image

In the docker folder, execute:
- docker-compose up - initialize all containers declared in the file
```

**Execute this commands on the project folder - Local**

```sh
In the docker folder, execute:
- docker-compose up -d mysql - initialize mysql container declared in the file

In the folder where is pom.xml, execute:
- mvn clean install - build project with your dependencies
- java -jar target/api-wallet-client.jar --spring.profiles.active=dev - start the project rest api on port 8080
```

**Testing the project - cURL or Postman or JMeter**

```sh
Post deposit (clientId: bb4d51cc-9a27-4963-8d12-7f42d1fd09b3)
- curl -X POST http://localhost:8080/wallet/bb4d51cc-9a27-4963-8d12-7f42d1fd09b3/deposit \
  -H 'Content-Type: application/json' \
  -d '{
    "amount": 300,
    "currency": "EUR"
}'

POST withdraw (clientId: bb4d51cc-9a27-4963-8d12-7f42d1fd09b3)

- curl -X POST http://localhost:8080/wallet/bb4d51cc-9a27-4963-8d12-7f42d1fd09b3/withdraw \
  -H 'Content-Type: application/json' \
  -d '{
    "amount": 100,
    "currency": "EUR"
}'

Get balance (clientId: bb4d51cc-9a27-4963-8d12-7f42d1fd09b3)
- curl http://localhost:8080/wallet/bb4d51cc-9a27-4963-8d12-7f42d1fd09b3/balance

```
